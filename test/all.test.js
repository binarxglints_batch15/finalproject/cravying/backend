const request = require("supertest");
const app = require("../index");
const { users } = require("../models");
const { generateToken, decodeToken } = require("../helpers/jwt");
const { response } = require("express");

let token;
let customerToken;

test("Generate token for user id = 2,", () => {
  expect(typeof generateToken({ id: 2 })).toBe("string");
});

test("Decode token for user id = 2,", () => {
  let token = generateToken({ id: 2 });
  expect(typeof decodeToken(token)).toBe("object");
  expect(decodeToken(token)).toMatchObject({ id: 2 });
});

describe("User sign up", () => {
  describe("Successfully create user", () => {
    it("Should return 201 and obj (user)", (done) => {
      let input = {
        name: "ucu",
        email: "ucu@gmail.com",
        password: "Yusril123@",
        confirmPassword: "Yusril123@",
        role: "customer",
        image:
          "https://res.cloudinary.com/cravyng/image/upload/v1638628389/exmf0il6mb1vrawk6wuz.png",
      };
      request(app)
        .post("/user/signup")
        .send(input)
        .then((response) => {
          const { body, status } = response;
          expect(status).toBe(201);

          done();
        })
        .catch((err) => {
          done(err);
        });
    });
  });

  describe("Failed to create User", () => {
    it("Should return 400 and error messages", (done) => {
      let input = {
        email: "",
        password: "",
      };

      request(app)
        .post("/user/signup")
        .send(input)
        .then((response) => {
          const { body, status } = response;
          expect(status).toBe(400);
          // expect(body).toHaveProperty("error");
          // expect(body).toHaveProperty("errorMessages");
          // expect(Array.isArray(body.errorMessages)).toBe(true);
          done();
        })
        .catch((err) => {
          done(err);
        });
    });
  });
});

// beforeAll(() => {
//   users.create({
//     name: "Muhammad Yusril",
//     email: "muhammadyusril147@gmail.com",
//     password: "yusril123@",
//     role: "customer",
//     image:
//       "https://res.cloudinary.com/cravyng/image/upload/v1638628389/exmf0il6mb1vrawk6wuz.png",
//   });
// });
// afterAll((done) => {
//   users
//     .destroy({ where: {} })
//     .then(() => {
//       done();
//     })
//     .catch((err) => {
//       done(err);
//     });
// });

describe("Merchant try to login:", () => {
  describe("Success:", () => {
    it("Should return 200 and token", (done) => {
      let input = {
        email: "kill13@gamil.com",
        password: "Yusril123@",
      };
      request(app)
        .post("/user/signin")
        .send(input)
        .then((response) => {
          let { body, status } = response;
          expect(status).toBe(200);
          expect(body).toHaveProperty("token");
          token = body.token;
          done();
        })
        .catch((err) => {
          done(err);
        });
    });
  });

  describe("Failed:", () => {
    describe("Wrong email", () => {
      it("Should return 401 and 'Email/password invalid'", (done) => {
        let input = {
          email: "kill13@gail.com",
          password: "Yusril123",
        };
        request(app)
          .post("/user/signin")
          .send(input)
          .then((response) => {
            let { body, status } = response;
            expect(status).toBe(401);
            done();
          })
          .catch((err) => {
            done(err);
          });
      });
    });

    describe("Wrong password", () => {
      it("Should return 400 and 'Email/password invalid'", (done) => {
        let input = {
          email: "kill13@gamil.com",
          password: "Yusril@",
        };
        request(app)
          .post("/user/signin")
          .send(input)
          .then((response) => {
            let { body, status } = response;
            expect(status).toBe(400);
            expect(body).toHaveProperty("message");
            expect(body.message).toEqual("Email/password invalid");
            done();
          })
          .catch((err) => {
            done(err);
          });
      });
    });
  });
});

describe("Customer try to login:", () => {
  describe("Success:", () => {
    it("Should return 200 and token", (done) => {
      let input = {
        email: "yusril@gmail.com",
        password: "Yusril123@",
      };
      request(app)
        .post("/user/signin")
        .send(input)
        .then((response) => {
          let { body, status } = response;
          console.log(body);
          expect(status).toBe(200);
          expect(body).toHaveProperty("token");
          customerToken = body.token;
          done();
        })
        .catch((err) => {
          done(err);
        });
    });
  });

  describe("Failed:", () => {
    describe("Wrong email", () => {
      it("Should return 401 and 'Email/password invalid'", (done) => {
        let input = {
          email: "yuil@gmail.com",
          password: "Yusril123",
        };
        request(app)
          .post("/user/signin")
          .send(input)
          .then((response) => {
            let { body, status } = response;
            expect(status).toBe(401);
            done();
          })
          .catch((err) => {
            done(err);
          });
      });
    });

    describe("Wrong password", () => {
      it("Should return 400 and 'Email/password invalid'", (done) => {
        let input = {
          email: "yusril@gmail.com",
          password: "Yusril@",
        };
        request(app)
          .post("/user/signin")
          .send(input)
          .then((response) => {
            let { body, status } = response;
            expect(status).toBe(400);
            expect(body).toHaveProperty("message");
            expect(body.message).toEqual("Email/password invalid");
            done();
          })
          .catch((err) => {
            done(err);
          });
      });
    });
  });
});

describe("User CRUD", () => {
  describe("Get all user data", () => {
    it("Should return 200 and array of user object", (done) => {
      request(app)
        .get("/user")
        .then((response) => {
          let { body, status } = response;
          expect(status).toBe(200);
          expect(Array.isArray(body.data)).toBe(true);
          done();
        })
        .catch((err) => {
          done(err);
        });
    });
  });
  describe("get detail User", () => {
    it("should return 200 and object of user", (done) => {
      request(app)
        .get("/user/myprofile")
        .set("token", `${token}`)
        .then((response) => {
          let { body, status } = response;
          expect(status).toBe(200);
          expect(body.data.name).toEqual("Aqilla");
          done();
        })
        .catch((err) => {
          done(err);
        });
    });
  });
  describe("get all sales summary", () => {
    it("should return 200 and object of data", (done) => {
      request(app)
        .get("/user/summary")
        .set("token", `${token}`)
        .then((response) => {
          let { status } = response;
          expect(status).toBe(200);
          done();
        })
        .catch((err) => {
          done(err);
        });
    });
  });
  describe("get all sales summary(filtered", () => {
    it("should return 200 and object of data", (done) => {
      request(app)
        .get("/user/summary?from=2021-12-27&to=2021-12-28")
        .set("token", `${token}`)
        .then((response) => {
          let { status } = response;
          expect(status).toBe(200);
          done();
        })
        .catch((err) => {
          done(err);
        });
    });
  });

  describe("get history customer", () => {
    it("should return 200 and object of data", (done) => {
      request(app)
        .get("/user/history")
        .set("token", `${token}`)
        .then((response) => {
          let { status } = response;
          expect(status).toBe(200);
          done();
        })
        .catch((err) => {
          done(err);
        });
    });
  });

  describe("Delete User", () => {
    describe("succes to delete user", () => {
      it("should return 200", (done) => {
        request(app)
          .delete("/user/164")
          .then((response) => {
            let { status } = response;
            expect(status).toBe(200);
            done();
          })
          .catch((err) => {
            done(err);
          });
      });
    });
    describe("failed to delete user", () => {
      it("should return 404", (done) => {
        request(app)
          .delete("/user/1000")
          .then((response) => {
            let { status } = response;
            expect(status).toBe(404);
            done();
          })
          .catch((err) => {
            done(err);
          });
      });
    });
  });
});

describe("Discount CRUD", () => {
  describe("get all discount", () => {
    it("Should return 200", (done) => {
      request(app)
        .get("/discount")
        .then((response) => {
          let { body, status } = response;
          expect(status).toBe(200);
          expect(Array.isArray(body.data)).toBe(true);
          done();
        })
        .catch((err) => {
          done(err);
        });
    });
  });

  describe("get detail discount", () => {
    describe("succes, ID is found", () => {
      it("should return 200", (done) => {
        request(app)
          .get("/discount/1")
          .then((response) => {
            let { body, status } = response;
            expect(status).toBe(200);
            expect(body.data.id).toBe(1);
            done();
          })
          .catch((err) => {
            done(err);
          });
      });
    });

    describe("failed, ID not found", () => {
      it("should return 404", (done) => {
        request(app)
          .get("/discount/1000")
          .then((response) => {
            let { body, status } = response;
            expect(status).toBe(404);
            expect(body).toHaveProperty("errors");
            done();
          })
          .catch((err) => {
            done(err);
          });
      });
    });
  });

  describe("create discount", () => {
    describe("succes to create discount", () => {
      it("should return status 201, and created a discount", (done) => {
        let input = {
          title: "50%",
          description: "50% off on minimum order value of 500.000",
          disc: "50",
          minOrderPrice: "500000",
          code: "cravyng50",
        };
        request(app)
          .post("/discount")
          .set("token", `${token}`)
          .send(input)
          .then((response) => {
            let { body, status } = response;
            expect(status).toBe(201);
            expect(body.message).toEqual(
              expect.arrayContaining(["Success create discount"])
            );
            done();
          })
          .catch((err) => {
            done(err);
          });
      });
    });
    describe("failed to create discount", () => {
      it("should return status 403, and created a discount", (done) => {
        let input = {
          title: "50%",
          description: "50% off on minimum order value of 500.000",
          disc: "50",
          minOrderPrice: "500000",
          code: "cravyng50",
        };
        request(app)
          .post("/discount")
          .set("token", `${customerToken}`)
          .send(input)
          .then((response) => {
            let { body, status } = response;
            expect(status).toBe(403);
            done();
          })
          .catch((err) => {
            done(err);
          });
      });
    });
    describe("failed to create discount", () => {
      it("should return status 500", (done) => {
        let input = {
          judul: "50",
          description: "50% off on minimum order value of 500.000",
          disc: "50",
          minOrderPrice: "500000",
          code: "cravyng50",
        };
        request(app)
          .post("/discount")
          .set("token", `${token}`)
          .send(input)
          .then((response) => {
            let { body, status } = response;
            expect(status).toBe(500);
            done();
          })
          .catch((err) => {
            done(err);
          });
      });
    });
  });

  describe("update discount", () => {
    it("should return status 201, and message Discount has been updated", (done) => {
      let input = {
        description: "50% off on minimum order value of 600.000",
        minOrderPrice: "600000",
      };
      request(app)
        .put("/discount/3")
        .set("token", `${token}`)
        .send(input)
        .then((response) => {
          let { body, status } = response;
          expect(status).toBe(201);
          expect(body.message).toEqual(
            expect.arrayContaining(["Discount has been updated"])
          );
          done();
        })
        .catch((err) => {
          done(err);
        });
    });
  });

  describe("delete discount", () => {
    describe("succes to delete discount", () => {
      it("should return status 200, and message Discount has been deleted", (done) => {
        request(app)
          .delete("/discount/58")
          .set("token", `${token}`)
          .then((response) => {
            let { body, status } = response;
            expect(status).toBe(200);
            expect(body.message).toEqual("Discount has been deleted");

            done();
          })
          .catch((err) => {
            done(err);
          });
      });
    });
    describe("failed to delete discount", () => {
      it("should return status 404", (done) => {
        request(app)
          .delete("/discount/1000")
          .set("token", `${token}`)
          .then((response) => {
            let { body, status } = response;
            expect(status).toBe(400);
            done();
          })
          .catch((err) => {
            done(err);
          });
      });
    });
  });
});

describe("Menu CRUD", () => {
  describe("get all menu", () => {
    it("should return 200", (done) => {
      request(app)
        .get("/menu/home")
        .then((response) => {
          let { body, status } = response;
          expect(status).toBe(200);
          expect(Array.isArray(body.data)).toBe(true);
          done();
        })
        .catch((err) => {
          done(err);
        });
    });
  });
  describe("get menu by categoryId", () => {
    describe("succes, categoryId is exist", () => {
      it("should return 200", (done) => {
        request(app)
          .get("/menu/category/1")
          .then((response) => {
            let { body, status } = response;
            expect(status).toBe(200);
            expect(Array.isArray(body.data)).toBe(true);
            done();
          })
          .catch((err) => {
            done(err);
          });
      });
    });
    describe("failed, categoryId is not exist", () => {
      it("should return 404", (done) => {
        request(app)
          .get("/menu/category/100")
          .then((response) => {
            let { body, status } = response;
            expect(status).toBe(404);
            expect(body.errors).toEqual(
              expect.arrayContaining(["Category menu not found"])
            );
            done();
          })
          .catch((err) => {
            done(err);
          });
      });
    });
  });

  describe("get Menu by Category name", () => {
    describe("succes, Category is exist", () => {
      it("should return 200", (done) => {
        request(app)
          .get("/menu/category?category=recommended")
          .then((response) => {
            let { body, status } = response;
            expect(status).toBe(200);
            expect(Array.isArray(body.data)).toBe(true);
            done();
          })
          .catch((err) => {
            done(err);
          });
      });
    });
    describe("failed, category is not exist", () => {
      it("should return 404", (done) => {
        request(app)
          .get("/menu/category?category=topfood")
          .then((response) => {
            let { body, status } = response;
            expect(status).toBe(404);
            expect(body).toHaveProperty("errors");
            expect(body.errors).toEqual(
              expect.arrayContaining(["menu not found"])
            );
            done();
          })
          .catch((err) => {
            done(err);
          });
      });
    });
  });

  describe("get detail menu", () => {
    describe("succes, menu exist", () => {
      it("should return 200", (done) => {
        request(app)
          .get("/menu/1")
          .then((response) => {
            let { body, status } = response;
            expect(status).toBe(200);
            expect(body).toHaveProperty("data");
            done();
          })
          .catch((err) => [done(err)]);
      });
    });

    describe("failed, menu does not exist", () => {
      it("should return 404", (done) => {
        request(app)
          .get("/menu/100")
          .then((response) => {
            let { body, status } = response;
            expect(status).toBe(404);
            expect(body.errors).toEqual(
              expect.arrayContaining(["Detail menu not found"])
            );
            done();
          })
          .catch((err) => {
            done(err);
          });
      });
    });
  });

  describe("Create Menu", () => {
    describe("succes to create menu", () => {
      it("should return status 201, and created menu", (done) => {
        let input = {
          categoryId: "1",
          food: "Ayam bakar",
          image:
            "https://res.cloudinary.com/cravyng/image/upload/v1640604878/mmkciq6wakasdugpxvya.png",
          price: "30000",
          specialPrice: "25000",
          description: "ayam bakar dengan harga terjangkau",
          variants: [
            {
              label: "sambal",
              options: ["sambal matah", "sambal bawang"],
            },
          ],
        };
        request(app)
          .post("/menu")
          .set("token", `${token}`)
          .send(input)
          .then((response) => {
            let { body, status } = response;
            expect(status).toBe(201);
            expect(body.message).toEqual(
              expect.arrayContaining(["Succes create menu"])
            );
            done();
          })
          .catch((err) => {
            done(err);
          });
      });
    });

    describe("failed to create menu", () => {
      it("should return status 500", (done) => {
        let input = {
          categoryId: "1",
          makan: "Ayam bakar",
          image:
            "https://res.cloudinary.com/cravyng/image/upload/v1640604878/mmkciq6wakasdugpxvya.png",
          price: "30000",
          specialPrice: "25000",
          description: "ayam bakar dengan harga terjangkau",
          variants: [
            {
              label: "sambal",
              options: ["sambal matah", "sambal bawang"],
            },
          ],
        };
        request(app)
          .post("/menu")
          .set("token", `${token}`)
          .send(input)
          .then((response) => {
            let { body, status } = response;
            expect(status).toBe(500);
            done();
          })
          .catch((err) => {
            done(err);
          });
      });
    });
    describe("input non exist categoryId", () => {
      it("should return status 400", (done) => {
        let input = {
          categoryId: "19",
          food: "Ayam bakar",
          image:
            "https://res.cloudinary.com/cravyng/image/upload/v1640604878/mmkciq6wakasdugpxvya.png",
          price: "30000",
          specialPrice: "25000",
          description: "ayam bakar dengan harga terjangkau",
          variants: [
            {
              label: "sambal",
              options: ["sambal matah", "sambal bawang"],
            },
          ],
        };
        request(app)
          .post("/menu")
          .set("token", `${token}`)
          .send(input)
          .then((response) => {
            let { body, status } = response;
            expect(status).toBe(400);
            done();
          })
          .catch((err) => {
            done(err);
          });
      });
    });
    describe("input is empty", () => {
      it("should return status 400", (done) => {
        let input = {
          categoryId: "",
          food: "",
          image:
            "https://res.cloudinary.com/cravyng/image/upload/v1640604878/mmkciq6wakasdugpxvya.png",
          price: "",
          specialPrice: "",
          description: "",
          variants: [
            {
              label: "sambal",
              options: ["sambal matah", "sambal bawang"],
            },
          ],
        };
        request(app)
          .post("/menu")
          .set("token", `${token}`)
          .send(input)
          .then((response) => {
            let { body, status } = response;
            expect(status).toBe(400);
            // expect(body.message).toEqual(
            //   expect.arrayContaining(["Succes create menu"])
            // );
            done();
          })
          .catch((err) => {
            done(err);
          });
      });
    });
    describe("failed to create menu", () => {
      it("should return 403", (done) => {
        let input = {
          categoryId: "1",
          food: "Ayam bakar",
          image:
            "https://res.cloudinary.com/cravyng/image/upload/v1640604878/mmkciq6wakasdugpxvya.png",
          price: "30000",
          specialPrice: "25000",
          description: "ayam bakar dengan harga terjangkau",
          variants: [
            {
              label: "sambal",
              options: ["sambal matah", "sambal bawang"],
            },
          ],
        };
        request(app)
          .post("/menu")
          .set("token", `${customerToken}`)
          .send(input)
          .then((response) => {
            let { body, status } = response;
            expect(status).toBe(403);
            expect(body.message).toEqual(
              "You must signup as a merchant for create menu"
            );
            done();
          })
          .catch((err) => {
            done(err);
          });
      });
    });
    describe("not signed in", () => {
      it("should return 401", (done) => {
        let input = {
          categoryId: "1",
          food: "Ayam bakar",
          image:
            "https://res.cloudinary.com/cravyng/image/upload/v1640604878/mmkciq6wakasdugpxvya.png",
          price: "30000",
          specialPrice: "25000",
          description: "ayam bakar dengan harga terjangkau",
          variants: [
            {
              label: "sambal",
              options: ["sambal matah", "sambal bawang"],
            },
          ],
        };
        request(app)
          .post("/menu")
          .send(input)
          .then((response) => {
            let { body, status } = response;
            expect(status).toBe(401);
            expect(body.message).toEqual("Please sign in first");
            done();
          })
          .catch((err) => {
            done(err);
          });
      });
    });
  });

  describe("delete menu", () => {
    describe("succes to delete menu", () => {
      it("should return 200", (done) => {
        request(app)
          .delete("/menu/46")
          .set("token", `${token}`)
          .then((response) => {
            let { body, status } = response;
            expect(status).toBe(200);
            expect(body.message).toEqual("Menu has been deleted");
            done();
          })
          .catch((err) => {
            done(err);
          });
      });
    });
    describe("failed to delete menu (unauthorized)", () => {
      it("should return 403", (done) => {
        request(app)
          .delete("/menu/5")
          .set("token", `${token}`)
          .then((response) => {
            let { body, status } = response;
            expect(status).toBe(403);
            done();
          })
          .catch((err) => {
            done(err);
          });
      });
    });
    describe("failed to delete, menu does not exist or has been deleted", () => {
      it("should return 404", (done) => {
        request(app)
          .delete("/menu/1000")
          .set("token", `${token}`)
          .then((response) => {
            let { body, status } = response;
            expect(status).toBe(404);
            // expect(body.message).toEqual("Menu has been deleted");
            done();
          })
          .catch((err) => {
            done(err);
          });
      });
    });

    describe("failed to delete menu", () => {
      it("should return 403", (done) => {
        request(app)
          .delete("/menu/14")
          .set("token", `${customerToken}`)
          .then((response) => {
            let { body, status } = response;
            expect(status).toBe(403);
            expect(body.message).toEqual(
              "You must signup as a merchant for delete menu"
            );
            done();
          })
          .catch((err) => {
            done(err);
          });
      });
    });

    describe("not signed in", () => {
      it("should return 401", (done) => {
        request(app)
          .delete("/menu/14")
          .then((response) => {
            let { body, status } = response;
            expect(status).toBe(401);
            expect(body.message).toEqual("Please sign in first");
            done();
          })
          .catch((err) => {
            done(err);
          });
      });
    });
  });
});

describe("orderMenu CRUD", () => {
  describe("get all cart", () => {
    it("should return 200", (done) => {
      request(app)
        .get("/ordersmenu")
        .then((response) => {
          let { body, status } = response;
          expect(status).toBe(200);
          expect(body).toHaveProperty("data");
          expect(Array.isArray(body.data)).toBe(true);
          done();
        })
        .catch((err) => {
          done(err);
        });
    });
  });

  describe("create order menu", () => {
    describe("succes to create order menu", () => {
      it("should return 201 and created an orderMenu", (done) => {
        let input = {
          orderId: "1",
          menuId: "1",
          variantId: "1",
          variantOptionId: "1",
          quantity: "2",
        };
        request(app)
          .post("/ordersmenu")
          .set("token", `${token}`)
          .send(input)
          .then((response) => {
            let { body, status } = response;
            expect(status).toBe(201);
            expect(body).toHaveProperty("data");
            done();
          })
          .catch((err) => {
            done(err);
          });
      });
    });
    describe("failed to create order menu", () => {
      it("should return 400 and error message", (done) => {
        let input = {
          orderId: "",
          menuId: "",
          variantId: "",
          variantOptionId: "",
          quantity: "",
        };
        request(app)
          .post("/ordersmenu")
          .set("token", `${token}`)
          .send(input)
          .then((response) => {
            let { body, status } = response;
            expect(status).toBe(400);
            // expect(body).toHaveProperty("data");
            done();
          })
          .catch((err) => {
            done(err);
          });
      });
    });
    describe("input the wrong input", () => {
      it("should return 400 and error message", (done) => {
        let input = {
          orderId: "satu",
          menuId: "satu",
          variantId: "satu",
          variantOptionId: "satu",
          quantity: "satu",
        };
        request(app)
          .post("/ordersmenu")
          .set("token", `${token}`)
          .send(input)
          .then((response) => {
            let { body, status } = response;
            expect(status).toBe(400);
            // expect(body).toHaveProperty("data");
            done();
          })
          .catch((err) => {
            done(err);
          });
      });
    });
  });

  describe("get detail OrderMenu", () => {
    describe("succes, order menu is exist", () => {
      it("should return 200 and object of data", (done) => {
        request(app)
          .get("/ordersmenu/1")
          .then((response) => {
            let { status } = response;
            expect(status).toBe(200);
            done();
          })
          .catch((err) => {
            done(err);
          });
      });
    });
    describe("failed, order menu is not exist", () => {
      it("should return 400 and error message", (done) => {
        request(app)
          .get("/ordersmenu/1000")
          .then((response) => {
            let { status } = response;
            expect(status).toBe(404);
            done();
          })
          .catch((err) => {
            done(err);
          });
      });
    });
  });

  describe("Update order menu", () => {
    describe("succes to update order menu", () => {
      it("should return 201", (done) => {
        let input = {
          orderId: "2",
          menuId: "2",
          variantId: "2",
          variantOptionId: "2",
          quantity: "2",
        };
        request(app)
          .put("/ordersmenu/2")
          .send(input)
          .then((response) => {
            let { status } = response;
            expect(status).toBe(201);
            done();
          })
          .catch((err) => {
            done(err);
          });
      });
    });
    describe("failed to update order menu(orderMenu is not exist)", () => {
      it("should return 404", (done) => {
        let input = {
          orderId: "2",
          menuId: "2",
          variantId: "2",
          variantOptionId: "2",
          quantity: "2",
        };
        request(app)
          .put("/ordersmenu/1000")
          .send(input)
          .then((response) => {
            let { status } = response;
            expect(status).toBe(400);
            done();
          })
          .catch((err) => {
            done(err);
          });
      });
    });
  });
  describe("Delete Order Menu", () => {
    describe("succes to delete order menu", () => {
      it("should return 200", (done) => {
        request(app)
          .delete("/ordersmenu/43")
          .then((response) => {
            let { status } = response;
            expect(status).toBe(200);
            done();
          })
          .catch((err) => {
            done(err);
          });
      });
    });
    describe("failed to delete orderMenu", () => {
      it("should return 404", (done) => {
        request(app)
          .delete("/ordersmenu/1000")
          .then((response) => {
            let { status } = response;
            expect(status).toBe(404);
            done();
          })
          .catch((err) => {
            done(err);
          });
      });
    });
  });
});

describe("Order CRUD", () => {
  describe("get all order", () => {
    describe("succes", () => {
      it("should return 200", (done) => {
        request(app)
          .get("/order")
          .then((response) => {
            let { status } = response;
            expect(status).toBe(200);
            done();
          })
          .catch((err) => {
            done(err);
          });
      });
    });
  });
  describe("get detail order", () => {
    describe("succes", () => {
      it("should return 200", (done) => {
        request(app)
          .get("/order/1")
          .then((response) => {
            let { status } = response;
            expect(status).toBe(200);
            done();
          })
          .catch((err) => {
            done(err);
          });
      });
    });
    describe("failed", () => {
      it("should return 404 order not found", (done) => {
        request(app)
          .get("/order/1000")
          .then((response) => {
            let { status } = response;
            expect(status).toBe(404);
            done();
          })
          .catch((err) => {
            done(err);
          });
      });
    });
  });
  describe("create Order", () => {
    describe("succes to create order", () => {
      it("should return 201", (done) => {
        let input = {
          rating: "4",
        };
        request(app)
          .post("/order")
          .set("token", `${token}`)
          .send(input)
          .then((response) => {
            let { status } = response;
            expect(status).toBe(201);
            done();
          })
          .catch((err) => {
            done(err);
          });
      });
    });
    describe("failed to create order", () => {
      it("should return 400", (done) => {
        let input = {
          rating: "empat",
        };
        request(app)
          .post("/order")
          .set("token", `${token}`)
          .send(input)
          .then((response) => {
            let { status } = response;
            expect(status).toBe(400);
            done();
          })
          .catch((err) => {
            done(err);
          });
      });
    });
  });

  describe("Update Order", () => {
    describe("succes to update order", () => {
      it("should return 201", (done) => {
        let input = {
          rating: "5",
        };
        request(app)
          .put("/order/1")
          .set("token", `${token}`)
          .send(input)
          .then((response) => {
            let { status } = response;
            expect(status).toBe(201);
            done();
          })
          .catch((err) => {
            done(err);
          });
      });
    });

    describe("apply voucher code", () => {
      it("should return 201", (done) => {
        let input = {
          voucherCode: "cek",
        };
        request(app)
          .put("/order/147")
          .set("token", `${token}`)
          .send(input)
          .then((response) => {
            let { status } = response;
            expect(status).toBe(201);
            done();
          })
          .catch((err) => {
            done(err);
          });
      });
    });

    describe("apply wrong voucher code", () => {
      it("should return 400", (done) => {
        let input = {
          voucherCode: "lala",
        };
        request(app)
          .put("/order/147")
          .set("token", `${token}`)
          .send(input)
          .then((response) => {
            let { status } = response;
            expect(status).toBe(400);
            done();
          })
          .catch((err) => {
            done(err);
          });
      });
    });
    describe("didnt met the minimum order value", () => {
      it("should return 400", (done) => {
        let input = {
          voucherCode: "cek",
        };
        request(app)
          .put("/order/200")
          .set("token", `${token}`)
          .send(input)
          .then((response) => {
            let { status } = response;
            expect(status).toBe(400);
            done();
          })
          .catch((err) => {
            done(err);
          });
      });
    });
    describe("failed to update order(order does not exist)", () => {
      it("should return 400", (done) => {
        let input = {
          rating: "5",
        };
        request(app)
          .put("/order/1000")
          .set("token", `${token}`)
          .send(input)
          .then((response) => {
            let { status } = response;
            expect(status).toBe(400);
            done();
          })
          .catch((err) => {
            done(err);
          });
      });
    });
    describe("failed to update order", () => {
      it("should return 500", (done) => {
        let input = {
          rating: "lima",
        };
        request(app)
          .put("/order/1")
          .set("token", `${token}`)
          .send(input)
          .then((response) => {
            let { status } = response;
            expect(status).toBe(500);
            done();
          })
          .catch((err) => {
            done(err);
          });
      });
    });
  });

  describe("Delete Order ", () => {
    describe("succes to delete order ", () => {
      it("should return 200", (done) => {
        request(app)
          .delete("/order/52")
          .then((response) => {
            let { status } = response;
            expect(status).toBe(200);
            done();
          })
          .catch((err) => {
            done(err);
          });
      });
    });
    describe("failed to delete order", () => {
      it("should return 404", (done) => {
        request(app)
          .delete("/order/1000")
          .then((response) => {
            let { status } = response;
            expect(status).toBe(404);
            done();
          })
          .catch((err) => {
            done(err);
          });
      });
    });
  });
});
