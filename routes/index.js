const router = require("express").Router();
const users = require("./users");
const menu = require("./menu");
const orders_menu = require("./orderMenu");
const orders = require("./orders");
const discount = require("./discount");

router.use("/user", users);
router.use("/menu", menu);
router.use("/ordersmenu", orders_menu);
router.use("/order", orders);
router.use("/discount", discount);

module.exports = router;
