const express = require("express");

const {
  createOrder,
  updateOrder,
  getAllOrders,
  getDetailOrder,
  deleteOrder,
} = require("../controllers/orders");

const { isSignedIn } = require("../middlewares/validators/auth");

const {
  createOrderValidator,
  updateOrderValidator,
} = require("../middlewares/validators/orders");

const router = express.Router();

router
  .route("/")
  .post(isSignedIn, createOrderValidator, createOrder)
  .get(getAllOrders);

router
  .route("/:id")
  .get(getDetailOrder)
  .put(isSignedIn, updateOrderValidator, updateOrder)
  .delete(deleteOrder);

module.exports = router;
