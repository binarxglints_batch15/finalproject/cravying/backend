const express = require("express");

const {
  createOrderMenuValidator,
  updateOrderMenuValidator,
} = require("../middlewares/validators/orderMenu");

const {
  getAllCart,
  getDetailOrderMenu,
  createorderMenu,
  updateOrderMenu,
  deleteOrderMenu,
} = require("../controllers/orderMenu");

const router = express.Router();

router
  .route("/")
  .get(getAllCart)
  .post(createOrderMenuValidator, createorderMenu);

router
  .route("/:id")
  .get(getDetailOrderMenu)
  .put(updateOrderMenuValidator, updateOrderMenu)
  .delete(deleteOrderMenu);

module.exports = router;
