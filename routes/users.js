const router = require("express").Router();
const { isSignedIn } = require("../middlewares/validators/auth");
const { checkout, callback } = require("../controllers/payment");
const passport = require("passport");
require("../middlewares/validators/googleOAuth");

// Import User Validator
const {
  createUserValidator,
  updateUserValidator,
} = require("../middlewares/validators/users");

// Import users controller
const {
  getAllUser,
  getDetailUser,
  signUp,
  signIn,
  updateUser,
  deleteUser,
  googleSignIn,
  failed,
} = require("../controllers/users");

const { getAllOrderMenu } = require("../controllers/orderMenu");
const { salesSummary } = require("../controllers/salesSummary");

router.get("/", getAllUser);
router.get("/history", isSignedIn, getAllOrderMenu);
router.get("/summary", isSignedIn, salesSummary);
router.get("/myprofile", getDetailUser);
router.post("/signup", createUserValidator, signUp);
router.post("/signin", signIn);
router.post("/payment/:id", checkout);
router.post("/callback", callback);
router.put("/", isSignedIn, updateUserValidator, updateUser);
router.delete("/:id", deleteUser);
router.get("/failed", failed);
router.get(
  "/auth/google",
  passport.authenticate("google", {
    session: false,
    scope: ["profile", "email"],
  })
);
router.get(
  "/auth/google/callback",
  passport.authenticate("google", {
    session: false,
    failureRedirect: "/failed",
  }),
  googleSignIn
);

// Export router
module.exports = router;
