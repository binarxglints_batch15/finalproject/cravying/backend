const { order, orderMenu, users, menu } = require("../models");
const sequelize = require("sequelize");
const moment = require("moment");
const { Op } = require("sequelize");

class Summary {
  async salesSummary(req, res, next) {
    try {
      // //   // YYYY-MM-DD
      const from = req.query.from;
      // // // YYYY-MM-DD
      const to = req.query.to;

      let options;
      if (from != null && to != null) {
        options = {
          createdAt: {},
        };
      } else {
        options = {};
      }

      if (from != null) {
        options.createdAt[Op.gte] = from;
      }

      if (to != null) {
        options.createdAt[Op.lte] = to;
      }

      let dataOne = await orderMenu.findAll({
        attributes: [
          [sequelize.literal("COUNT(DISTINCT(orderId))"), "TotalTransactions"],
          [sequelize.fn("sum", sequelize.col("subTotalPrice")), "gross Sales"],
          [sequelize.fn("sum", sequelize.col("subTotalPrice")), "net Sales"],
          [
            sequelize.fn("AVG", sequelize.col("subTotalPrice")),
            "Average Sales Per Transaction",
          ],
        ],
        where: options,
        raw: true,
        include: [
          {
            //join table menu
            model: menu,
            where: { userId: req.userData.id },
            attributes: ["userId"],
            include: [
              {
                //join table user
                model: users,
                attributes: ["name"],
              },
            ],
          },
        ],
      });
      let paidTransactions = await order.findAll({
        where: { status: "paid" },
        attributes: [
          [
            sequelize.fn("sum", sequelize.col("priceTotal")),
            "paid Transactions",
          ],
        ],
        raw: true,
      });

      let unPaidTransactions = await order.findAll({
        where: { status: "unpaid" },
        attributes: [
          [
            sequelize.fn("sum", sequelize.col("priceTotal")),
            "unpaid Transactions",
          ],
        ],
        raw: true,
      });

      let data = await orderMenu.findAll({
        // logging: console.log,
        attributes: [
          "orderId",
          [sequelize.fn("sum", sequelize.col("subTotalPrice")), "totalOrder"],
        ],
        group: ["orderMenu.orderId"],
        raw: true,

        include: [
          {
            //join table menu
            model: menu,
            where: { userId: req.userData.id },
            attributes: ["userId"],
            include: [
              {
                //join table user
                model: users,
                attributes: ["name"],
              },
            ],
          },
          {
            //join table order
            model: order,
            where: options,
            attributes: ["status", "createdAt"],
          },
        ],
        order: [["order", "createdAt", "DESC"]],
      });

      if (dataOne.length === 0 && data.length === 0) {
        return res.status(404).json({ errors: ["No Orders Found"] });
      }

      let rating = await order.findAll({
        attributes: [
          [sequelize.fn("AVG", sequelize.col("rating")), "Average Rating"],
        ],
      });

      res.status(200).json({
        rating,
        dataOne,
        paidTransactions,
        unPaidTransactions,
        data,
      });
    } catch (error) {
      return res.status(500).json({ errors: ["Internal Server Error"] });
    }
  }
}

module.exports = new Summary();
