const validator = require("validator");
const ordersMenu = require("../../controllers/orderMenu");
const { menu, orderMenu } = require("../../models");

exports.createOrderMenuValidator = async (req, res, next) => {
  try {
    const errors = [];
    // validate if the inputted menuId is integer or not
    if (!validator.isInt(req.body.menuId)) {
      errors.push("menuId must be a number");
    }
    // validate if the inputted orderId is integer or not
    if (!validator.isInt(req.body.orderId)) {
      errors.push("orderId must be a number");
    }
    // if variantId is inputted
    if (req.body.variantId) {
      // validate if the inputted variantId is integer or not
      if (!validator.isInt(req.body.variantId)) {
        errors.push("variantId must be a number");
      }

      if (errors.length > 0) {
        return res.status(400).json({ errors: errors });
      }
    }
    // if variantOptionId is inputted
    if (req.body.variantOptionId) {
      // validate if the inputted variantOptionId is integer or not
      if (!validator.isInt(req.body.variantOptionId)) {
        errors.push("variantOptionId must be a number");
      }

      if (errors.length > 0) {
        return res.status(400).json({ errors: errors });
      }
    }
    // validate if the inputted quantity is integer or not
    if (!validator.isInt(req.body.quantity)) {
      errors.push("rating must be a number");
    }

    if (errors.length > 0) {
      return res.status(400).json({ errors: errors });
    }

    const findMenu = await menu.findOne({ where: { id: req.body.menuId } });
    if (!findMenu) {
      errors.push("Menu does not exist");
    }

    if (errors.length > 0) {
      return res.status(400).json({ errors: errors });
    }

    const { price, specialPrice } = findMenu; //to get the price and specialPrice from Menu

    if (specialPrice !== null) {
      req.body.subTotalPrice =
        parseFloat(req.body.quantity) * parseFloat(specialPrice);
    } else {
      req.body.subTotalPrice =
        parseFloat(req.body.quantity) * parseFloat(price);
    }

    next();
  } catch (error) {
    return res.status(400).json({ errors: ["Bad Request"] });
  }
};

exports.updateOrderMenuValidator = async (req, res, next) => {
  try {
    const errors = [];

    if (req.body.menuId) {
      if (!validator.isInt(req.body.menuId)) {
        errors.push("menuId must be a number");
      }
    }

    if (req.body.orderId) {
      if (!validator.isInt(req.body.orderId)) {
        errors.push("orderId must be a number");
      }
    }

    if (req.body.variantId) {
      if (!validator.isInt(req.body.variantId)) {
        errors.push("variantId must be a number");
      }

      if (errors.length > 0) {
        return res.status(400).json({ errors: errors });
      }
    }

    if (req.body.variantOptionId) {
      if (!validator.isInt(req.body.variantOptionId)) {
        errors.push("variantOptionId must be a number");
      }

      if (errors.length > 0) {
        return res.status(400).json({ errors: errors });
      }
    }

    if (!validator.isInt(req.body.quantity)) {
      errors.push("rating must be a number");
    }

    const findOrderMenu = await orderMenu.findOne({
      where: { id: req.params.id },
    });
    if (!findOrderMenu) {
      errors.push("orderMenu not found");
    }
    const findMenu = await menu.findOne({
      where: { id: findOrderMenu.dataValues.menuId },
    });
    if (!findMenu) {
      errors.push("Menu does not exist");
    }

    if (errors.length > 0) {
      return res.status(400).json({ errors: errors });
    }

    const { price, specialPrice } = findMenu.dataValues;

    if (specialPrice !== null) {
      req.body.subTotalPrice =
        parseFloat(req.body.quantity) * parseFloat(specialPrice);
    } else {
      req.body.subTotalPrice =
        parseFloat(req.body.quantity) * parseFloat(price);
    }

    next();
  } catch (error) {
    return res.status(400).json({ errors: ["Bad Request"] });
  }
};
