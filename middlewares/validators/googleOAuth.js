const path = require("path");
require("dotenv").config({ path: `.env.${process.env.NODE_ENV}` });
const GoogleStrategy = require("passport-google-oauth20").Strategy;
const passport = require("passport");
const { users } = require("../../models");

passport.serializeUser(function (user, done) {
  done(null, user);
});

passport.deserializeUser(function (user, done) {
  done(null, user);
});
passport.use(
  new GoogleStrategy(
    {
      clientID: process.env.GOOGLE_CLIENT_ID,
      clientSecret: process.env.GOOGLE_CLIENT_SECRET,
      callbackURL: process.env.GOOGLE_CALLBACK_URL,
    },
    async (accessToken, refreshToken, profile, cb) => {
      try {
        //first it will find whether the email is already registered in database or not
        let googleLogin = await users.findOne({
          where: {
            email: profile.emails[0].value,
          },
        });
        //if not registered yet, it will create first and then return the callback url
        if (!googleLogin) {
          googleLogin = await users.create({
            name: profile.displayName,
            email: profile.emails[0].value,
            password: "Google Password",
            role: "customer",
          });
        }

        return cb(null, googleLogin);
      } catch (error) {
        return cb(null, false);
      }
    }
  )
);
